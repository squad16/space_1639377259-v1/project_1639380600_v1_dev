from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='project_1639380600_v1_dev',
    packages=find_packages(where='src', exclude=['tests']),
    package_dir={'': 'src'},
    version=version,
    description='project_13_12_21_1',
    author='Po Po'
)
